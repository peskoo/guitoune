### Guitoune

Merge request event in your webex chat

#### Configuration
- setup a gitlab webook on your group or project with mere_request_event.  
- Use the labels you put in config.json in your project.  
- Add team members to use randomness to pick a reviewer.

create config.json file at root project
```json
{
    "host": "127.0.0.1",
    "port": 8080,
    "cert": "just_the_name_of_your_file_in_tls_folder",
    "key": "just_the_name_of_your_file_in_tls_folder",
    "webex": {
        "token": "xxxx",
        "room_name": "La taverne des chenapans", // Keep "General" if it's your main channel",
        "email_domain": "xxxx.com",
        "team": {
            "name": "email@test.com"
        } 
    },
    "labels": {
        "for_review": "For Review",
        "comments": "Comments",
        "approved": "Approved"
    } 
}
```

To add or modify messages, see src/utils/mod.rs

#### TLS
Create tls folder with `mkdir tls` then add your cert and key into it and do not forget to add the name in your config.json file.  
For local you can use [mkcert](https://github.com/FiloSottile/mkcert) then create your own key and certificate into tls folder.  

#### Curl
`curl --header "Content-Type: application/json" -d @example_payload.json -X POST https://127.0.0.1:8080/mr_event --cacert tls/xxx.pem --cert tls/xxx.pem --key tls/xxx.pem`

#### Container

```bash
$ docker build -t guitoune .
$ docker run -ti -p8080:8080 --name guitoune -d guitoune
```
