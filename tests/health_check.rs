use guitoune::startup::run;
use reqwest;
use std::net::TcpListener;

// Launch our application in the background
fn spawn_app() -> String {
    let listener = TcpListener::bind("127.0.0.1:0").expect("Failed to bind random port");
    // We retrieve the port assigned to us by the OS
    let port = listener.local_addr().unwrap().port();
    let server = run(listener).expect("Failed to bind address");

    // Launch the server as a background task
    // tokio::spawn returns a handle to the spawned future,
    // but we have no use for it here, hence the non-binding let
    let _ = tokio::spawn(server);

    format!("127.0.0.1:{}", port)
}

// Test ignore, we must add cert with Identity and ClientBuilder to make a https reqwest.
#[ignore]
#[actix_web::test]
async fn test_health_check_200() {
    let client = reqwest::Client::new();
    let response = client
        .get(format!("https://{}/health_check", spawn_app()))
        .send()
        .await
        .expect("Failed to execute request.");

    // Assert
    assert!(response.status().is_success());
    assert_eq!(Some(0), response.content_length());
}
