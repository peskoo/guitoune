use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use std::fs::File;
use std::io::Read;
use std::net::Ipv4Addr;

// Config
use lazy_static::lazy_static;

lazy_static! {
    pub static ref CONFIG: Configuration = get_config();
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Configuration {
    pub host: Ipv4Addr,
    pub cert: String,
    pub key: String,
    pub port: u32,
    pub webex: WebexConf,
    pub labels: LabelsConf,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct WebexConf {
    pub token: String,
    pub room_name: String,
    pub email_domain: String,
    pub team: HashMap<String, String>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct LabelsConf {
    pub for_review: String,
    pub comments: String,
    pub approved: String,
}

pub fn get_config() -> Configuration {
    // Load Config
    let mut file = File::open("config.json").expect("File does not exists.");
    let mut config_to_convert = String::new();
    file.read_to_string(&mut config_to_convert)
        .expect("Error during converting json to string.");
    let _config: Configuration =
        serde_json::from_str(&config_to_convert).expect("Configuration file error.");

    _config
}
