use rand::seq::SliceRandom;

use crate::gitlab::json_struct::{Assignee, Reviewer};
use crate::webex::mention_to;

//                                                     //
//            ---- FORMAT MESSAGES ----                //
// This is here to add messages to your bot to answer. //
//                                                     //

pub fn format_markdown_message(
    state: u8,
    assignees: &[Assignee],
    mr_title: &String,
    mr_url: &String,
    reviewers: &Vec<Reviewer>,
) -> String {
    let peache = emojis::get("🍑").unwrap();
    let rocket = emojis::get("🚀").unwrap();
    let green_heart = emojis::get("💚").unwrap();
    let eyes = emojis::get("👀").unwrap();

    let mut all_reviewers = String::new();

    for reviewer in reviewers {
        all_reviewers.push_str(format!("{} ", mention_to(&reviewer.name)).as_str());
    }
    let assignee = assignees[0].name.clone();

    match state {
        0 => {
            let messages: Vec<String> = Vec::from([
                format!(
                    "{emoji} Nouvelle MR de {assignee} disponible sur les internets pour {reviewers} -> [{title}]({url})",
                    emoji=peache,
                    assignee=mention_to(&assignee),
                    reviewers=all_reviewers,
                    title=mr_title,
                    url=mr_url
                )
            ]);
            String::from(messages.choose(&mut rand::thread_rng()).unwrap())
        }
        1 => {
            let messages: Vec<String> = Vec::from([
                format!(
                    "{emoji} {assignee} vient de passer sa merge request en attente pour {reviewers} -> [{title}]({url})",
                    emoji=rocket,
                    assignee=mention_to(&assignee),
                    reviewers=all_reviewers,
                    title=mr_title,
                    url=mr_url
                ),
                format!(
                    "{emoji} {assignee} n'a pas le temps de niaiser, {reviewers} c'est pour toi -> [{title}]({url})",
                    emoji=rocket,
                    assignee=mention_to(&assignee),
                    reviewers=all_reviewers,
                    title=mr_title,
                    url=mr_url
                )
            ]);

            String::from(messages.choose(&mut rand::thread_rng()).unwrap())
        }
        2 => {
            let messages: Vec<String> = Vec::from([
                format!(
                    "{emoji} {reviewers} vient d'ajouter de nouveaux commentaires, à ton tour {assignee} -> [{title}]({url})",
                    emoji=eyes,
                    reviewers=all_reviewers,
                    assignee=mention_to(&assignee),
                    title=mr_title,
                    url=mr_url,
                ),
            ]);

            String::from(messages.choose(&mut rand::thread_rng()).unwrap())
        }
        3 => {
            let messages: Vec<String> = Vec::from([
                format!(
                    "{emoji} {reviewers} vient d'approuver ton chef d'oeuvre {assignee} -> [{title}]({url})",
                    emoji=green_heart,
                    reviewers=all_reviewers,
                    assignee=mention_to(&assignee),
                    title=mr_title,
                    url=mr_url,
                ),
                format!(
                    "{emoji} {reviewers} vient de vendre la caravane en validant ta MR {assignee} -> [{title}]({url})",
                    emoji=green_heart,
                    reviewers=all_reviewers,
                    assignee=mention_to(&assignee),
                    title=mr_title,
                    url=mr_url,
                )
            ]);

            String::from(messages.choose(&mut rand::thread_rng()).unwrap())
        }
        _ => String::from("This state is not set."),
    }
}
