use guitoune::configuration::CONFIG;
use guitoune::startup::run;
use std::net::TcpListener;

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    let address = format!("{}:{}", CONFIG.host, CONFIG.port);
    let listener = TcpListener::bind(address).expect("Failed to bind random port");

    run(listener)?.await
}
