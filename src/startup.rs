// Actix
use actix_web::dev::Server;
use actix_web::web;
use actix_web::{middleware::Logger, App, HttpServer};
use openssl::ssl::{SslAcceptor, SslFiletype, SslMethod};

use std::net::TcpListener;

use crate::configuration::CONFIG;
use crate::routes::health_check::health_check;
use crate::routes::merge_request::merge_request_event;

pub fn run(listener: TcpListener) -> Result<Server, std::io::Error> {
    env_logger::init_from_env(env_logger::Env::default().default_filter_or("info"));
    let key = format!("tls/{}", CONFIG.key);
    let cert = format!("tls/{}", CONFIG.cert);

    let mut builder = SslAcceptor::mozilla_intermediate(SslMethod::tls()).unwrap();
    builder.set_private_key_file(key, SslFiletype::PEM).unwrap();
    builder.set_certificate_chain_file(cert).unwrap();

    let server = HttpServer::new(|| {
        App::new()
            .route("/health_check", web::get().to(health_check))
            .route("/mr_event", web::post().to(merge_request_event))
            .wrap(Logger::new("%a %r %s %b %{Referer}i"))
    })
    .listen_openssl(listener, builder)?
    .run();

    Ok(server)
}
