use crate::configuration::CONFIG;

pub fn format_email(name: &String) -> String {
    // Gitlab name fit with first part of email address.
    // It should not work for everyone.

    for (key, value) in &CONFIG.webex.team {
        // Return an email in exceptions configuration directly.
        if name == key {
            return value.to_string();
        }
    }

    format!("{}@{}", name, CONFIG.webex.email_domain)
}

pub fn mention_to(name: &String) -> String {
    // Mention People with its webex ID or Email
    // "<@personId:123456789012345677890> "
    // "<@personEmail:xxxx@xxxx.com> "

    format!("<@personEmail:{}>", format_email(name))
}

async fn get_room_id() -> String {
    let webex = webex::Webex::new(CONFIG.webex.token.as_str()).await;
    let rooms = webex.get_all_rooms().await.expect("Error obtaning rooms");
    let mut result = String::new();

    for room in rooms {
        if room.title == CONFIG.webex.room_name {
            result = room.id;
        }
    }
    result
}

pub async fn send_private_message(to_email: String) {
    let webex = webex::Webex::new(CONFIG.webex.token.as_str()).await;
    let text = format!("Hello, *{}*", to_email);

    let msg_to_send = webex::types::MessageOut {
        to_person_email: Some(to_email),
        markdown: Some(text),
        ..Default::default()
    };

    webex.send_message(&msg_to_send).await.unwrap();
}

pub async fn send_room_message(message: String) {
    let webex = webex::Webex::new(CONFIG.webex.token.as_str()).await;
    let room_id: String = get_room_id().await;

    let msg_to_send = webex::types::MessageOut {
        room_id: Some(room_id),
        markdown: Some(message),
        ..Default::default()
    };

    webex.send_message(&msg_to_send).await.unwrap();
}
