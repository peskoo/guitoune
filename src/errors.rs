use actix_web::error;
use actix_web::http::header::ContentType;
use actix_web::http::StatusCode;
use actix_web::HttpResponse;
use derive_more::{Display, Error};

#[derive(PartialEq, Debug, Display, Error)]
pub enum AppError {
    #[display(fmt = "Data received are not correct.")]
    SerializeDataError,
    #[display(fmt = "No update, label is already here.")]
    LabelError,
}

impl error::ResponseError for AppError {
    fn error_response(&self) -> HttpResponse {
        HttpResponse::build(self.status_code())
            .insert_header(ContentType::html())
            .body(self.to_string())
    }

    fn status_code(&self) -> StatusCode {
        match *self {
            AppError::SerializeDataError => StatusCode::INTERNAL_SERVER_ERROR,
            AppError::LabelError => StatusCode::INTERNAL_SERVER_ERROR,
        }
    }
}
