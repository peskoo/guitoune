use actix_web::HttpResponse;

// Log
use log::{info, warn};
use rand::seq::IteratorRandom;

// Local
use crate::configuration::CONFIG;
use crate::errors::AppError;
use crate::gitlab::check::load_merge_request_struct;
use crate::gitlab::json_struct::{Assignee, Reviewer};
use crate::utils::format_markdown_message;
use crate::webex::{format_email, send_room_message};

fn get_reviewers(assignees: &Vec<Assignee>, reviewers: Vec<Reviewer>) -> Vec<Reviewer> {
    // Random reviewer if reviewers is None

    // Remove assignee from the team to avoid to select them as random reviewers.
    let mut team = CONFIG.webex.team.clone();
    for assignee in assignees {
        team.retain(|name, email| {
            info!("{} / {}", *name, *email);

            *name != assignee.name
        });
    }

    if reviewers.is_empty() {
        if let Some(team_name) = team.keys().choose(&mut rand::thread_rng()) {
            vec![Reviewer {
                name: team_name.to_string(),
                email: format_email(team_name),
            }]
        } else {
            warn!("Reviewer is empty and no team members are added to configuration.");
            Vec::new()
        }
    } else {
        info!("Reviewer is not empty.");
        reviewers
    }
}

pub async fn merge_request_event(req_body: String) -> Result<HttpResponse, AppError> {
    // Check Struct received from gitlab //
    let (action, assignees, reviewers, currents, previous, title, url) =
        load_merge_request_struct(&req_body)?;

    let reviewers = get_reviewers(&assignees, reviewers);

    // action -> open/update/close
    if action == "open" {
        info!(" ----- MR opened: ");
        let msg: String = format_markdown_message(0, &assignees, &title, &url, &reviewers);
        send_room_message(msg).await;
    }

    if action == "update" {
        info!(" ----- MR updated: ");

        // If previous contains one of current labels -> do nothing
        // If previous contains none of current labels -> message
        for label in previous {
            if currents.contains(&label) {
                info!(
                    "No need to send messages, label {} is already here.",
                    label.title
                );
                return Err(AppError::LabelError);
            }
        }

        let _for_review: &String = &CONFIG.labels.for_review;
        let _comments: &String = &CONFIG.labels.comments;
        let _approved: &String = &CONFIG.labels.approved;

        for current_label in currents {
            if &current_label.title == _for_review {
                info!("MR in review");
                let msg: String = format_markdown_message(1, &assignees, &title, &url, &reviewers);
                send_room_message(msg).await;
            } else if &current_label.title == _comments {
                info!("MR with comments");
                let msg: String = format_markdown_message(2, &assignees, &title, &url, &reviewers);
                send_room_message(msg).await;
            } else if &current_label.title == _approved {
                info!("MR Approved");
                let msg: String = format_markdown_message(3, &assignees, &title, &url, &reviewers);
                send_room_message(msg).await;
            } else {
                info!("MR unknown label: {}", current_label.title);
            }
        }
    }

    Ok(HttpResponse::Ok().finish())
}
