use log::{error, info};

use crate::errors::AppError;

use super::json_struct::{Assignee, Label, MergeRequestStruct, Reviewer};

#[allow(clippy::type_complexity)]
pub fn load_merge_request_struct(
    req_body: &str,
) -> Result<
    (
        String,
        Vec<Assignee>,
        Vec<Reviewer>,
        Vec<Label>,
        Vec<Label>,
        String,
        String,
    ),
    AppError,
> {
    let data: MergeRequestStruct = serde_json::from_str(req_body).map_err(|e| {
        error!("Serializing data error.: {}", e);
        AppError::SerializeDataError
    })?;
    info!("Data received from Gitlab: {:#?}", data);

    // Check Struct received from gitlab //
    let assignees = match data.assignees {
        None => {
            error!("Assignees: {:#?}", data.assignees);
            return Err(AppError::SerializeDataError);
        }
        Some(x) => x,
    };

    let reviewers = match data.reviewers {
        None => {
            error!("Reviewers: {:#?}", data.reviewers);
            return Err(AppError::SerializeDataError);
        }
        Some(x) => x,
    };

    let current_labels = match data.changes.labels {
        None => {
            error!("Labels changes: {:#?}", data.changes.labels);
            return Err(AppError::SerializeDataError);
        }
        Some(x) => x,
    };

    let currents = match current_labels.current {
        None => {
            error!("Current labels: {:#?}", current_labels.current);
            return Err(AppError::SerializeDataError);
        }
        Some(x) => x,
    };
    let previous = match current_labels.previous {
        None => {
            error!("Previous labels: {:#?}", current_labels.previous);
            return Err(AppError::SerializeDataError);
        }
        Some(x) => x,
    };

    Ok((
        data.object_attributes.action,
        assignees,
        reviewers,
        currents,
        previous,
        data.object_attributes.title,
        data.object_attributes.url,
    ))
}

#[cfg(test)]
mod tests {
    #[allow(unused_imports)]
    use super::*;

    #[test]
    fn test_ok_load_merge_request_struct() -> Result<(), AppError> {
        let req_body = String::from(
            "{
                \"object_attributes\": {
                    \"action\": \"coucou\",
                    \"title\": \"I am the MR test\",
                    \"url\": \"https://coucou.com\"
                },
                \"changes\": {
                    \"labels\": {
                        \"previous\": [
                            {\"title\": \"Avancement::For Review\"}
                        ],
                        \"current\": [
                            {\"title\": \"Avancement::For Review\"},
                            {\"title\": \"NO MERGE\"}
                        ]
                    }
                },
                \"assignees\": [
                    {
                        \"name\": \"luffy\",
                        \"email\": \"luffy@boat.com\"
                    }
                ],
                \"reviewers\": [
                    {
                        \"name\": \"zorro\",
                        \"email\": \"zorro@boat.com\"
                    }
                ]
            }",
        );

        let action = String::from("coucou");

        let assignees = vec![Assignee {
            name: "luffy".to_string(),
            email: "luffy@boat.com".to_string(),
        }];
        let reviewers = vec![Reviewer {
            name: "zorro".to_string(),
            email: "zorro@boat.com".to_string(),
        }];
        let currents = vec![
            Label {
                title: "Avancement::For Review".to_string(),
            },
            Label {
                title: "NO MERGE".to_string(),
            },
        ];
        let previous = vec![Label {
            title: "Avancement::For Review".to_string(),
        }];
        let title = "I am the MR test".to_string();
        let url = "https://coucou.com".to_string();

        let func = load_merge_request_struct(&req_body)?;
        let result = (action, assignees, reviewers, currents, previous, title, url);

        assert_eq!(func, result);

        Ok(())
    }

    #[test]
    fn test_error_load_merge_request_struct() {
        let req_body_incomplete = String::from(
            "{
                \"object_attributes\": {
                    \"title\": \"I am the MR test\",
                    \"url\": \"https://coucou.com\"
                }
            }",
        );

        assert_eq!(
            load_merge_request_struct(&req_body_incomplete),
            Err(AppError::SerializeDataError)
        );
    }
}
