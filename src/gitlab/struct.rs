// Handle Json request
use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize)]
pub struct MergeRequestStruct {
    // changes -> labels -> current
    // obj_attribute -> title
    // obj_attribute -> url
    // assignees
    // reviewers
    pub changes: Changes,
    pub object_attributes: ObjectAttributes,
    #[serde(default = "default_reviewers")]
    pub reviewers: Option<Vec<Reviewer>>,
    #[serde(default = "default_assignees")]
    pub assignees: Option<Vec<Assignee>>,
}
#[derive(Debug, Serialize, Deserialize)]
pub struct Changes {
    #[serde(default = "default_current")]
    pub labels: Option<CurrentLabels>,
}
#[derive(Debug, Serialize, Deserialize)]
pub struct CurrentLabels {
    pub current: Option<Vec<Label>>,
    pub previous: Option<Vec<Label>>,
}
#[derive(Debug, Serialize, Deserialize)]
pub struct ObjectAttributes {
    pub title: String,
    pub url: String,
}
#[derive(PartialEq, Debug, Serialize, Deserialize)]
pub struct Label {
    pub title: String,
}
#[derive(Debug, Serialize, Deserialize)]
pub struct Reviewer {
    pub name: String,
    pub email: String,
}
#[derive(Debug, Serialize, Deserialize)]
pub struct Assignee {
    pub name: String,
    pub email: String,
}

fn default_current() -> Option<CurrentLabels> {
    Some(CurrentLabels {
        current: Some(Vec::new()),
        previous: Some(Vec::new()),
    })
}
fn default_reviewers() -> Option<Vec<Reviewer>> {
    Some(Vec::new())
}

fn default_assignees() -> Option<Vec<Assignee>> {
    Some(Vec::new())
}
